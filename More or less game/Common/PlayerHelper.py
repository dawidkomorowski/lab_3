from numpy import random

from Game.GamePlayer import GameXPlayer, GameOPlayer, UnknownGamePlayerTypeException
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException


class PlayerHelper:
    @staticmethod
    def nextRandomPlayer():
        return random.choice([GameXPlayer, GameOPlayer])

    @staticmethod
    def oppositePlayer(gamePlayer):
        if gamePlayer is GameOPlayer:
            return GameXPlayer
        if gamePlayer is GameXPlayer:
            return GameOPlayer

        raise UnknownGamePlayerTypeException(gamePlayer)

    @staticmethod
    def isMessageForPlayer(player, currentPlayer, gameRuleException):
        if gameRuleException is None:
            return False

        if isinstance(gameRuleException, PlayerWonGameRuleException):
            return True

        if isinstance(gameRuleException, DrawGameRuleException):
            return True

        return player == currentPlayer
