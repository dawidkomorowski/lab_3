from unittest import TestCase

from Common.ResponseHelper import ResponseHelper
from Common.ResponseTypes import ResponseTypes
from Dto.GameMoveDto import GameMoveDto
from Dto.GameMoveDtoConverter import GameMoveDtoConverter
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Game.GameMove import GameMove
from Game.GamePlayer import GameOPlayer
from Network.Response import Response, ResponseStatus
from Xml.XmlSerializer import XmlSerializer


class TestResponseHelper(TestCase):
    def test_ShouldCreateResponseRaiseException_GivenUnknownType(self):
        # arrange
        type = -1

        # act
        with self.assertRaises(Exception) as cm:
            ResponseHelper.createResponse(type)

        ex = cm.exception;

        # assert
        self.assertEqual(ex.message, "Unknown type: " + str(type))

    def test_ShouldCreateResponse_GivenPlainOkResponseType(self):
        # arrange
        type = ResponseTypes.PlainOkResponse

        # act
        response = ResponseHelper.createResponse(type)

        # assert
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status, ResponseStatus.Ok)

    def test_ShouldCreateResponseRaiseException_GivenGameMoveResponseTypeWithNoArgument(self):
        # arrange
        type = ResponseTypes.GameMoveResponse

        # act
        with self.assertRaises(Exception) as cm:
            ResponseHelper.createResponse(type)

        ex = cm.exception;

        # assert
        self.assertEqual(ex.message, "Missing argument.")

    def test_ShouldCreateResponse_GivenGameMoveResponseType(self):
        # arrange
        type = ResponseTypes.GameMoveResponse
        data = GameMove(GameOPlayer, (1, 2))

        # act
        response = ResponseHelper.createResponse(type, data)

        gamePlayerDtoConverter = GamePlayerDtoConverter()
        gameMoveDtoConverter = GameMoveDtoConverter(gamePlayerDtoConverter)
        gameMoveDto = XmlSerializer.deserialize(response.data, GameMoveDto)
        gameMove = gameMoveDtoConverter.fromDto(gameMoveDto)

        # assert
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status, ResponseStatus.Ok)
        self.assertIsInstance(gameMove, GameMove)
        self.assertEqual(gameMove.gamePlayer, GameOPlayer)
        self.assertEqual(gameMove.targetXYPosition, (1, 2))

    def test_ShouldExtractDataRaiseException_GivenUnknownType(self):
        # arrange
        type = -1
        response = Response.create()

        # act
        with self.assertRaises(Exception) as cm:
            ResponseHelper.extractData(response, type)

        ex = cm.exception

        # assert
        self.assertEqual(ex.message, "Unknown type: " + str(type))

    def test_ShouldExtractData_GivenPlainOkResponseType(self):
        # arrange
        type = ResponseTypes.PlainOkResponse
        response = Response.create()

        # act
        extractedData = ResponseHelper.extractData(response, type)

        # assert
        self.assertEqual(extractedData, None)

    def test_ShouldExtractData_GivenGameMoveResponseType(self):
        # arrange
        type = ResponseTypes.GameMoveResponse
        gameMove = GameMove(GameOPlayer, (1, 2))
        response = ResponseHelper.createResponse(type, gameMove)

        # act
        extractedData = ResponseHelper.extractData(response, type)

        # assert
        self.assertEqual(extractedData.gamePlayer, gameMove.gamePlayer)
        self.assertEqual(extractedData.targetXYPosition, gameMove.targetXYPosition)
