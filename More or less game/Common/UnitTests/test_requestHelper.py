from unittest import TestCase

from Common.RequestHeaders import RequestHeaders
from Common.RequestHelper import RequestHelper
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Dto.GameStateDto import GameStateDto
from Dto.GameStateDtoConverter import GameStateDtoConverter
from Game.GameField import GameEmptyField, GameOField, GameXField
from Game.GamePlayer import GameOPlayer
from Game.GameState import GameState
from Network.Request import Request
from Xml.XmlSerializer import XmlSerializer


class TestRequestHelper(TestCase):
    def test_ShouldCreateRequestRaiseException_GivenUnknownHeader(self):
        # arrange
        header = "Unknown"

        # act
        with self.assertRaises(Exception) as cm:
            RequestHelper.createRequest(header)

        ex = cm.exception;

        # assert
        self.assertEqual(ex.message, "Unknown header: " + header)

    def test_ShouldCreateRequestRaiseException_GivenDisplayPlainTextHeaderWithNoArgument(self):
        # arrange
        header = RequestHeaders.DisplayPlainText

        # act
        with self.assertRaises(Exception) as cm:
            RequestHelper.createRequest(header)

        ex = cm.exception;

        # assert
        self.assertEqual(ex.message, "Missing argument.")

    def test_ShouldCreateRequest_GivenDisplayPlainTextHeader(self):
        # arrange
        header = RequestHeaders.DisplayPlainText
        data = "Plain text to be displayed."

        # act
        request = RequestHelper.createRequest(header, data)

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.header, RequestHeaders.DisplayPlainText)
        self.assertEqual(request.data, data)

    def test_ShouldCreateRequestRaiseException_GivenDisplayGameStateHeaderWithNoArgument(self):
        # arrange
        header = RequestHeaders.DisplayGameState

        # act
        with self.assertRaises(Exception) as cm:
            RequestHelper.createRequest(header)

        ex = cm.exception;

        # assert
        self.assertEqual(ex.message, "Missing argument.")

    def test_ShouldCreateRequest_GivenDisplayGameStateHeader(self):
        # arrange
        header = RequestHeaders.DisplayGameState
        data = GameState()
        data.currentPlayer = GameOPlayer
        data.map = [[GameXField, GameEmptyField, GameOField],
                    [GameOField, GameXField, GameEmptyField],
                    [GameEmptyField, GameOField, GameXField]]

        # act
        request = RequestHelper.createRequest(header, data)

        gamePlayerDtoConverter = GamePlayerDtoConverter()
        gameFieldDtoConverter = GameFieldDtoConverter()
        gameStateDtoConverter = GameStateDtoConverter(gamePlayerDtoConverter, gameFieldDtoConverter)
        gameStateDto = XmlSerializer.deserialize(request.data, GameStateDto)
        gameState = gameStateDtoConverter.fromDto(gameStateDto)

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.header, RequestHeaders.DisplayGameState)
        self.assertEqual(data.currentPlayer, gameState.currentPlayer)
        for x in range(0, 3):
            for y in range(0, 3):
                self.assertEqual(data.getFieldAt(x, y), gameState.getFieldAt(x, y))

    def test_ShouldCreateRequest_GivenAskForGameMoveHeader(self):
        # arrange
        header = RequestHeaders.AskForGameMove

        # act
        request = RequestHelper.createRequest(header)

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.header, RequestHeaders.AskForGameMove)

    def test_ShouldCreateRequest_GivenEndGameSessionHeader(self):
        # arrange
        header = RequestHeaders.EndGameSession

        # act
        request = RequestHelper.createRequest(header)

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.header, RequestHeaders.EndGameSession)

    def test_ShouldExtractDataRaiseException_GivenRequestWithUnknownHeader(self):
        # arrange
        header = "Unknown"
        request = Request.create(header=header)

        # act
        with self.assertRaises(Exception) as cm:
            RequestHelper.extractData(request)

        ex = cm.exception

        # assert
        self.assertEqual(ex.message, "Unknown header: " + header)

    def test_ShouldExtractData_GivenRequestWithDisplayPlainTextHeader(self):
        # arrange
        header = RequestHeaders.DisplayPlainText
        data = "Plain text to be displayed."
        request = RequestHelper.createRequest(header, data)

        # act
        extractedData = RequestHelper.extractData(request)

        # assert
        self.assertEqual(extractedData, data)

    def test_ShouldExtractData_GivenRequestWithDisplayGameStateHeader(self):
        # arrange
        header = RequestHeaders.DisplayGameState
        data = GameState()
        data.currentPlayer = GameOPlayer
        data.map = [[GameXField, GameEmptyField, GameOField],
                    [GameOField, GameXField, GameEmptyField],
                    [GameEmptyField, GameOField, GameXField]]

        request = RequestHelper.createRequest(header, data)

        # act
        extractedData = RequestHelper.extractData(request)

        # assert
        self.assertEqual(extractedData.currentPlayer, data.currentPlayer)
        for x in range(0, 3):
            for y in range(0, 3):
                self.assertEqual(extractedData.getFieldAt(x, y), data.getFieldAt(x, y))
