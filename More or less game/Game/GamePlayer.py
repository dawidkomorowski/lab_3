class GamePlayer:
    pass


class GameXPlayer(GamePlayer):
    pass


class GameOPlayer(GamePlayer):
    pass


class UnknownGamePlayerTypeException(Exception):
    def __init__(self, gamePlayer):
        self.gamePlayer = gamePlayer
        self.message = "Unknown game player type: " + str(gamePlayer)
