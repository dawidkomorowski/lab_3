from Game.GameRules.AfterMovePerspective.AfterMovePerspective import AfterMovePerspective
from Game.GameRules.Common import Common
from Game.GameRulesEngine.GameRule import GameRule, GameRuleException


class PlayerWonGameRule(GameRule):
    def __init__(self, gameFieldTypeMapper):
        self.perspective = AfterMovePerspective
        self._gameFieldTypeMapper = gameFieldTypeMapper

    def run(self, context):
        winner = Common.winner(context.gameState, self._gameFieldTypeMapper)
        if winner != None:
            raise PlayerWonGameRuleException(winner)


class PlayerWonGameRuleException(GameRuleException):
    def __init__(self, winner):
        self.winner = winner
