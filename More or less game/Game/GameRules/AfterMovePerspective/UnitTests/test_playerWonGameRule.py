from unittest import TestCase

from Game.GameField import GameOField, GameXField, GameEmptyField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer, GameOPlayer
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRule, PlayerWonGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestPlayerWonGameRule(TestCase):
    def test_ShouldNotRaiseException_WhenNoPlayerWon(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldNotRaiseException_WhenGameMapIsEmpty(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameEmptyField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameEmptyField, GameEmptyField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldRaiseException_WhenXPlayerWonHorizontally(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameXField, GameXField, GameXField],
                         [GameOField, GameXField, GameEmptyField],
                         [GameOField, GameEmptyField, GameOField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        with self.assertRaises(PlayerWonGameRuleException) as cm:
            gameRule.run(gameRuleContext)

        ex = cm.exception

        # assert
        self.assertEqual(ex.winner, GameXPlayer)

    def test_ShouldRaiseException_WhenXPlayerWonVertically(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameXField, GameXField, GameOField],
                         [GameOField, GameXField, GameEmptyField],
                         [GameOField, GameXField, GameOField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        with self.assertRaises(PlayerWonGameRuleException) as cm:
            gameRule.run(gameRuleContext)

        ex = cm.exception

        # assert
        self.assertEqual(ex.winner, GameXPlayer)

    def test_ShouldRaiseException_WhenOPlayerWonDiagonally(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameXField],
                         [GameOField, GameOField, GameEmptyField],
                         [GameXField, GameEmptyField, GameOField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        with self.assertRaises(PlayerWonGameRuleException) as cm:
            gameRule.run(gameRuleContext)

        ex = cm.exception

        # assert
        self.assertEqual(ex.winner, GameOPlayer)

    def test_ShouldRaiseException_WhenXPlayerWonVerticallyInLastColumn(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = PlayerWonGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameOField, GameXField],
                         [GameOField, GameOField, GameXField],
                         [GameXField, GameEmptyField, GameXField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        with self.assertRaises(PlayerWonGameRuleException) as cm:
            gameRule.run(gameRuleContext)

        ex = cm.exception

        # assert
        self.assertEqual(ex.winner, GameXPlayer)
