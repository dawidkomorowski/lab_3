from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRules.Common import Common
from Game.GameRulesEngine.GameRule import GameRule, GameRuleException


class ValidateGameNotFinishedGameRule(GameRule):
    def __init__(self):
        self.perspective = BeforeMovePerspective

    def run(self, context):
        if Common.gameFinished(context.gameState):
            raise GameAlreadyFinishedGameRuleException()


class GameAlreadyFinishedGameRuleException(GameRuleException):
    pass
