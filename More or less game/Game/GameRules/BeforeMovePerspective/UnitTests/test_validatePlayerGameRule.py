from unittest import TestCase

from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer, GameOPlayer
from Game.GameRules.BeforeMovePerspective.ValidatePlayerGameRule import ValidatePlayerGameRule, \
    InvalidPlayerGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestValidatePlayerGameRule(TestCase):
    def test_ShouldNotRaiseException_GivenValidPlayer(self):
        # arrange
        gameRule = ValidatePlayerGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameMove = GameMove(GameXPlayer)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldRaiseException_GivenInvalidPlayer(self):
        # arrange
        gameRule = ValidatePlayerGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameMove = GameMove(GameOPlayer)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        with self.assertRaises(InvalidPlayerGameRuleException):
            gameRule.run(gameRuleContext)
