from unittest import TestCase

from Game.GameField import GameOField, GameEmptyField, GameXField
from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer
from Game.GameRules.BeforeMovePerspective.ValidateGameNotFinishedGameRule import ValidateGameNotFinishedGameRule, \
    GameAlreadyFinishedGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestValidateGameNotFinished(TestCase):
    def test_ShouldNotRaiseException_GivenGameStateWithEmptyField(self):
        # arrange
        gameRule = ValidateGameNotFinishedGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldRaiseException_GivenGameStateWithNoEmptyField(self):
        # arrange
        gameRule = ValidateGameNotFinishedGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameOField, GameXField],
                         [GameXField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        with self.assertRaises(GameAlreadyFinishedGameRuleException):
            gameRule.run(gameRuleContext)
