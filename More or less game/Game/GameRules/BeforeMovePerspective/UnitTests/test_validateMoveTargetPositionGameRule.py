from unittest import TestCase

from Game.GameField import GameEmptyField, GameOField, GameXField
from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import ValidateMoveTargetPositionGameRule, \
    InvalidTargetPositionGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestValidateMoveTargetPositionGameRule(TestCase):
    def test_ShouldNotRaiseException_GivenCorrectTargetPosition(self):
        # arrange
        gameRule = ValidateMoveTargetPositionGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldRaiseException_GivenIncorrectTargetPosition(self):
        # arrange
        gameRule = ValidateMoveTargetPositionGameRule()
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer, (0, 0))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        with self.assertRaises(InvalidTargetPositionGameRuleException):
            gameRule.run(gameRuleContext)
