from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRulesEngine.GameRule import GameRule, GameRuleException


class ValidatePlayerGameRule(GameRule):
    def __init__(self):
        self.perspective = BeforeMovePerspective

    def run(self, context):
        if context.gameState.currentPlayer != context.gameMove.gamePlayer:
            raise InvalidPlayerGameRuleException()


class InvalidPlayerGameRuleException(GameRuleException):
    pass
