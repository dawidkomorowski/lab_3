from Game.GameField import GameEmptyField
from Game.GamePlayer import GameXPlayer, GameOPlayer, UnknownGamePlayerTypeException


class Common:
    @staticmethod
    def gameFinished(gameState):
        for row in gameState.map:
            if GameEmptyField in row:
                return False
        return True

    @staticmethod
    def winner(gameState, gameFieldTypeMapper):
        for i in range(0, 3):
            if gameState.getFieldAt(0, i) != GameEmptyField:
                if gameState.getFieldAt(0, i) == gameState.getFieldAt(1, i) == gameState.getFieldAt(2, i):
                    winner = gameFieldTypeMapper.toGamePlayer(gameState.getFieldAt(0, i))
                    return winner
            if gameState.getFieldAt(i, 0) != GameEmptyField:
                if gameState.getFieldAt(i, 0) == gameState.getFieldAt(i, 1) == gameState.getFieldAt(i, 2):
                    winner = gameFieldTypeMapper.toGamePlayer(gameState.getFieldAt(i, 0))
                    return winner

        if gameState.getFieldAt(1, 1) != GameEmptyField:
            if gameState.getFieldAt(0, 0) == gameState.getFieldAt(1, 1) == gameState.getFieldAt(2, 2):
                winner = gameFieldTypeMapper.toGamePlayer(gameState.getFieldAt(0, 0))
                return winner

            if gameState.getFieldAt(0, 2) == gameState.getFieldAt(1, 1) == gameState.getFieldAt(2, 0):
                winner = gameFieldTypeMapper.toGamePlayer(gameState.getFieldAt(0, 0))
                return winner

        return None

    @staticmethod
    def oppositePlayer(gamePlayer):
        if gamePlayer == GameXPlayer:
            return GameOPlayer
        if gamePlayer == GameOPlayer:
            return GameXPlayer

        raise UnknownGamePlayerTypeException(gamePlayer)
