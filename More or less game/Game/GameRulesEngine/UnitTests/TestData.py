from Game.GameRulesEngine.GameRule import GameRule


class MockRule(GameRule):
    def __init__(self, perspective):
        self.perspective = perspective
        self.context = None

    def run(self, context):
        self.context = context
