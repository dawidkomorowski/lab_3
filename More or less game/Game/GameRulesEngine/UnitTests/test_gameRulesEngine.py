from unittest import TestCase

from Game.GameRules.AfterMovePerspective.AfterMovePerspective import AfterMovePerspective
from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRulesEngine.GameRule import GameRule
from Game.GameRulesEngine.GameRuleContext import GameRuleContext
from Game.GameRulesEngine.GameRulesEngine import GameRulesEngine
from Game.GameRulesEngine.UnitTests.TestData import MockRule


class TestGameRulesEngine(TestCase):
    def test_ShouldRunRulesOfGivenPerspective(self):
        # arrange
        gameRulesEngine = GameRulesEngine()
        gameRuleContext = GameRuleContext(None, None)
        ruleOfCorrectPerspective1 = MockRule(BeforeMovePerspective)
        ruleOfCorrectPerspective2 = MockRule(BeforeMovePerspective)
        ruleOfIncorrectPerspective1 = MockRule(AfterMovePerspective)
        ruleOfIncorrectPerspective2 = MockRule(AfterMovePerspective)

        gameRulesEngine.addRule(ruleOfCorrectPerspective1)
        gameRulesEngine.addRule(ruleOfCorrectPerspective2)
        gameRulesEngine.addRule(ruleOfIncorrectPerspective1)
        gameRulesEngine.addRule(ruleOfIncorrectPerspective2)

        gameRulesEngine.setContext(gameRuleContext)

        # act
        gameRulesEngine.run(BeforeMovePerspective)

        # assert
        self.assertEqual(ruleOfCorrectPerspective1.context, gameRuleContext)
        self.assertEqual(ruleOfCorrectPerspective2.context, gameRuleContext)
        self.assertEqual(ruleOfIncorrectPerspective1.context, None)
        self.assertEqual(ruleOfIncorrectPerspective2.context, None)

    def test_ShouldAddOneRule(self):
        # arrange
        gameRulesEngine = GameRulesEngine()
        rule = GameRule()

        # act
        gameRulesEngine.addRule(rule)

        # assert
        self.assertEqual(len(gameRulesEngine._rules), 1)
        self.assertEqual(gameRulesEngine._rules[0], rule)

    def test_ShouldSetContext(self):
        # arrange
        gameRulesEngine = GameRulesEngine()
        gameRuleContext = GameRuleContext(None, None)

        # act
        gameRulesEngine.setContext(gameRuleContext)

        # assert
        self.assertEqual(gameRulesEngine._context, gameRuleContext)
