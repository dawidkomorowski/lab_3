from unittest import TestCase

from Game.GameMove import GameMove
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestGameRuleContextBuilder(TestCase):
    def test_ShouldReturnEmptyContextBuilder(self):
        # arrange
        gameRuleContextBuilder = GameRuleContextBuilder()

        # act
        emptyGameRuleContextBuilder = gameRuleContextBuilder.context()

        # assert
        self.assertEqual(emptyGameRuleContextBuilder, gameRuleContextBuilder)
        self.assertEqual(emptyGameRuleContextBuilder._gameState, None)
        self.assertEqual(emptyGameRuleContextBuilder._gameMove, None)

    def test_ShouldBuildEmptyContext(self):
        # arrange
        gameRuleContextBuilder = GameRuleContextBuilder()

        # act
        gameRuleContext = gameRuleContextBuilder.context().build()

        # assert
        self.assertEqual(gameRuleContext.gameState, None)
        self.assertEqual(gameRuleContext.gameMove, None)

    def test_ShouldBuildContextWithGameState(self):
        # arrange
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState()

        # act
        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).build()

        # assert
        self.assertEqual(gameRuleContext.gameState, gameState)
        self.assertEqual(gameRuleContext.gameMove, None)

    def test_ShouldBuildContextWithGameMove(self):
        # arrange
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameMove = GameMove()

        # act
        gameRuleContext = gameRuleContextBuilder.context().withGameMove(gameMove).build()

        # assert
        self.assertEqual(gameRuleContext.gameState, None)
        self.assertEqual(gameRuleContext.gameMove, gameMove)

    def test_ShouldReturnContextWithGameStateAndGameMove(self):
        # arrange
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState()
        gameMove = GameMove()

        # act
        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # assert
        self.assertEqual(gameRuleContext.gameState, gameState)
        self.assertEqual(gameRuleContext.gameMove, gameMove)
