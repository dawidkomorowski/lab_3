from abc import abstractmethod


class GameRule:
    def __init__(self):
        self.perspective = None

    @abstractmethod
    def run(self, context):
        pass


class GameRuleException(Exception):
    pass
