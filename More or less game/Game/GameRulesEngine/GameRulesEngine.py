import logging


class GameRulesEngine:
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.logger.debug("Initializing game rules engine.")

        self._rules = ()
        self._context = None

    def run(self, perspective):
        for rule in self._rules:
            if rule.perspective is perspective:
                self.logger.debug("In perspective: " + str(perspective) + " running game rule: " + str(rule))
                rule.run(self._context)

    def addRule(self, rule):
        self.logger.debug("New game rule added to game rules engine. New rule is: " + str(rule))
        self._rules = self._rules + (rule,)

    def setContext(self, context):
        self._context = context
