from Game.GameRulesEngine.GameRuleContext import GameRuleContext


class GameRuleContextBuilder:
    def __init__(self):
        self._gameState = None
        self._gameMove = None

    def context(self):
        self._gameState = None
        self._gameMove = None
        return self

    def withGameState(self, gameState):
        self._gameState = gameState
        return self

    def withGameMove(self, gameMove):
        self._gameMove = gameMove
        return self

    def build(self):
        return GameRuleContext(self._gameState, self._gameMove)
