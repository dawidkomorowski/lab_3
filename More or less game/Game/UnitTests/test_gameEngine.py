from unittest import TestCase

from Game.GameEngine import GameEngine
from Game.GameField import GameEmptyField, GameXField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GameMove import GameMove
from Game.GamePlayer import GameOPlayer, GameXPlayer
from Game.GameRules.AfterMovePerspective.AfterMovePerspective import AfterMovePerspective
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRule
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRule
from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRules.BeforeMovePerspective.ValidateGameNotFinishedGameRule import ValidateGameNotFinishedGameRule
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import ValidateMoveTargetPositionGameRule
from Game.GameRules.BeforeMovePerspective.ValidatePlayerGameRule import ValidatePlayerGameRule, \
    InvalidPlayerGameRuleException
from Game.GameRules.MakeMovePerspective.MakeMoveGameRule import MakeMoveGameRule
from Game.GameRules.MakeMovePerspective.MakeMovePerspective import MakeMovePerspective
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameRulesEngine.GameRulePerspectiveConfigurationBuilder import GameRulePerspectiveConfigurationBuilder
from Game.GameRulesEngine.GameRulesEngine import GameRulesEngine
from Game.GameState import GameState


class TestGameEngine(TestCase):
    def test_ShouldTransformGameStateAndShouldReturnNone(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()

        gameRulesEngine = GameRulesEngine()
        gameRulesEngine.addRule(ValidateMoveTargetPositionGameRule())
        gameRulesEngine.addRule(ValidatePlayerGameRule())
        gameRulesEngine.addRule(ValidateGameNotFinishedGameRule())
        gameRulesEngine.addRule(MakeMoveGameRule(gameFieldTypeMapper))
        gameRulesEngine.addRule(PlayerWonGameRule(gameFieldTypeMapper))
        gameRulesEngine.addRule(DrawGameRule(gameFieldTypeMapper))

        gameRuleContextBulder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)

        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()
        gameRulePerspectiveConfiguration = gameRulePerspectiveConfigurationBuilder.configuration().withPerspective(
                BeforeMovePerspective).withPerspective(MakeMovePerspective).withPerspective(
                AfterMovePerspective).build()

        gameEngine = GameEngine(gameRulesEngine, gameRuleContextBulder, gameState, gameRulePerspectiveConfiguration)
        gameMove = GameMove(GameXPlayer, (0, 0))

        expectedGameStateMap = [[GameXField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField]]

        # act
        ex = gameEngine.proceedGame(gameMove)

        # assert
        self.assertEqual(gameState.map, expectedGameStateMap)
        self.assertEqual(ex, None)

    def test_ShouldNotTransformGameStateAndShouldNotReturnNone(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()

        gameRulesEngine = GameRulesEngine()
        gameRulesEngine.addRule(ValidateMoveTargetPositionGameRule())
        gameRulesEngine.addRule(ValidatePlayerGameRule())
        gameRulesEngine.addRule(ValidateGameNotFinishedGameRule())
        gameRulesEngine.addRule(MakeMoveGameRule(gameFieldTypeMapper))
        gameRulesEngine.addRule(PlayerWonGameRule(gameFieldTypeMapper))
        gameRulesEngine.addRule(DrawGameRule(gameFieldTypeMapper))

        gameRuleContextBulder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)

        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()
        gameRulePerspectiveConfiguration = gameRulePerspectiveConfigurationBuilder.configuration().withPerspective(
                BeforeMovePerspective).withPerspective(MakeMovePerspective).withPerspective(
                AfterMovePerspective).build()

        gameEngine = GameEngine(gameRulesEngine, gameRuleContextBulder, gameState, gameRulePerspectiveConfiguration)
        gameMove = GameMove(GameOPlayer, (0, 0))

        expectedGameStateMap = [[GameEmptyField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField]]

        # act
        ex = gameEngine.proceedGame(gameMove)

        # assert
        self.assertEqual(gameState.map, expectedGameStateMap)
        self.assertIsInstance(ex, InvalidPlayerGameRuleException)
