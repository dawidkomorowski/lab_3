from Game.GameField import GameEmptyField
from Game.GamePlayer import GameXPlayer


class GameState:
    def __init__(self, currentPlayer=GameXPlayer):
        self.currentPlayer = currentPlayer
        self.map = [[GameEmptyField, GameEmptyField, GameEmptyField],
                    [GameEmptyField, GameEmptyField, GameEmptyField],
                    [GameEmptyField, GameEmptyField, GameEmptyField]]

    def getFieldAt(self, x, y):
        return self.map[y][x];

    def setFieldAt(self, x, y, field):
        self.map[y][x] = field
