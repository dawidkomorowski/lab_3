import logging

from Game.GameRulesEngine.GameRule import GameRuleException


class GameEngine:
    logger = logging.getLogger(__name__)

    def __init__(self, gameRulesEngine, gameRuleContextBuilder, gameState, gameRulePerspectiveConfiguration):
        self.logger.debug("Initializing game engine.")

        self._gameRulesEngine = gameRulesEngine
        self._gameRuleContextBuilder = gameRuleContextBuilder
        self._gameState = gameState
        self._gameRulePerspectiveConfiguration = gameRulePerspectiveConfiguration

    def proceedGame(self, gameMove):
        self.logger.debug("Building game rule context.")

        gameRuleContext = self._gameRuleContextBuilder.context().withGameState(self._gameState).withGameMove(
                gameMove).build()
        self._gameRulesEngine.setContext(gameRuleContext)

        try:
            for perspective in self._gameRulePerspectiveConfiguration.perspectivesInExecutionOrder:
                self._gameRulesEngine.run(perspective)
        except GameRuleException as ex:
            self.logger.info("Rule chain execution interupted by game rule exception: " + str(ex.__class__))
            return ex

        self.logger.info("Game rules for all perspectives have been run.")
        return None
