import logging

from Game.AbstractGame import AbstractGame
from Game.GameEngine import GameEngine
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GamePlayer import GameXPlayer
from Game.GameRules.AfterMovePerspective.AfterMovePerspective import AfterMovePerspective
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRule
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRule
from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRules.BeforeMovePerspective.ValidateGameNotFinishedGameRule import ValidateGameNotFinishedGameRule
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import \
    ValidateMoveTargetPositionGameRule
from Game.GameRules.BeforeMovePerspective.ValidatePlayerGameRule import ValidatePlayerGameRule
from Game.GameRules.MakeMovePerspective.MakeMoveGameRule import MakeMoveGameRule
from Game.GameRules.MakeMovePerspective.MakeMovePerspective import MakeMovePerspective
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameRulesEngine.GameRulePerspectiveConfigurationBuilder import GameRulePerspectiveConfigurationBuilder
from Game.GameRulesEngine.GameRulesEngine import GameRulesEngine
from Game.GameState import GameState


class TicTacToeGame(AbstractGame):
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.reset()

    def reset(self):
        self.logger.debug("Starting Tic Tac Toe game initialization.")

        self._gameFieldTypeMapper = GameFieldTypeMapper()

        self._gameRulesEngine = GameRulesEngine()
        self._gameRulesEngine.addRule(ValidateMoveTargetPositionGameRule())
        self._gameRulesEngine.addRule(ValidatePlayerGameRule())
        self._gameRulesEngine.addRule(ValidateGameNotFinishedGameRule())
        self._gameRulesEngine.addRule(MakeMoveGameRule(self._gameFieldTypeMapper))
        self._gameRulesEngine.addRule(PlayerWonGameRule(self._gameFieldTypeMapper))
        self._gameRulesEngine.addRule(DrawGameRule(self._gameFieldTypeMapper))

        self._gameRuleContextBulder = GameRuleContextBuilder()
        self._gameState = GameState(GameXPlayer)

        self.logger.debug("Building game rule perspective configuration.")

        self._gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()
        self._gameRulePerspectiveConfiguration = self._gameRulePerspectiveConfigurationBuilder.configuration().withPerspective(
                BeforeMovePerspective).withPerspective(MakeMovePerspective).withPerspective(
                AfterMovePerspective).build()

        for perspective in self._gameRulePerspectiveConfiguration.perspectivesInExecutionOrder:
            self.logger.debug("Perspective added to configuration: " + str(perspective))

        self._gameEngine = GameEngine(self._gameRulesEngine, self._gameRuleContextBulder, self._gameState,
                                      self._gameRulePerspectiveConfiguration)

        self.logger.debug("Finished Tic Tac Toe game initialization.")

    def makeMove(self, gameMove):
        self.logger.debug("Passing game move to game engine.")

        return self._gameEngine.proceedGame(gameMove)
