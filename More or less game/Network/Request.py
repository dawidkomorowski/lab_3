from Xml.XmlSerializer import XmlValue


class Request:
    data = XmlValue(str)
    header = XmlValue(str)

    @staticmethod
    def create(data="", header=""):
        request = Request()
        request.data = data
        request.header = header
        return request
