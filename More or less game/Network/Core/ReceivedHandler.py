from abc import abstractmethod


class ReceivedHandler:
    @abstractmethod
    def handle(self, data):
        pass
