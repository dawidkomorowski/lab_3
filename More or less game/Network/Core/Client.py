import Queue
import logging
import select
import socket
from time import sleep

from Network.Core.Configuration import Configuration
from Threading.UnitOfWork import UnitOfWork
from Threading.UnitOfWorkThread import UnitOfWorkThread


class Client:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration=Configuration()):
        self.logger.debug("Initializing client.")

        self._configuration = configuration
        self._timeout = self._configuration.connectTimeout

        self._sendQueue = Queue.Queue()

        self.logger.debug("Creating client units of work.")
        self._receiveThread = UnitOfWorkThread(ClientReceiveUnitOfWork(self))
        self._sendThread = UnitOfWorkThread(ClientSendUnitOfWork(self))

        self.logger.debug("Creating client socket.")
        self._socket = socket.socket()
        self._socket.settimeout(self._timeout)

        self.receivedHandler = None

    def start(self):
        self.logger.debug("Starting client.")

        self.logger.debug("Connecting client socket to server.")
        self._socket.connect((self._configuration.serverAddress, self._configuration.port))

        self._timeout = self._configuration.timeout
        self._socket.settimeout(self._timeout)

        self._socket.setblocking(0)
        self.logger.debug("Starting receive unit of work thread.")
        self._receiveThread.start()
        self.logger.debug("Starting send unit of work thread.")
        self._sendThread.start()

    def stop(self):
        self.logger.debug("Stopping client.")

        self.logger.debug("Stopping receive unit of work thread.")
        self._sendThread.stop()
        self._sendThread.join(self._timeout)

        self.logger.debug("Stopping send unit of work thread.")
        self._receiveThread.stop()
        self._receiveThread.join(self._timeout)

        self._socket.shutdown(socket.SHUT_RDWR)
        sleep(1)

        self.logger.debug("Closing client socket.")
        self._socket.close()

    def send(self, data):
        self._sendQueue.put(data)


class ClientReceiveUnitOfWork(UnitOfWork):
    def __init__(self, client):
        self._client = client

    def execute(self):
        socket = self._client._socket

        try:
            ready = select.select([socket], [], [], self._client._timeout)
            if ready[0]:
                data = socket.recv(self._client._configuration.packetSize)
                if not data: return
                if self._client.receivedHandler != None:
                    self._client.receivedHandler.handle(data)
        except:
            socket.close()
            raise


class ClientSendUnitOfWork(UnitOfWork):
    def __init__(self, client):
        self._client = client

    def execute(self):
        socket = self._client._socket

        try:
            if not self._client._sendQueue.empty():
                data = self._client._sendQueue.get()
                socket.sendall(data)
        except:
            socket.close()
            raise
