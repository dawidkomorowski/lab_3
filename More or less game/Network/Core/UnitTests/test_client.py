from _socket import error
from unittest import TestCase

from Network.Core.Client import Client
from Network.Core.Configuration import Configuration


class TestClient(TestCase):
    def test_ShouldRaiseTimeout_WhenNoServer(self):
        # arrange
        configuration = Configuration()
        configuration.timeout = 0.01
        client = Client(configuration)

        # act
        # assert
        with self.assertRaises(error):
            client.start()

    def test_ShouldPutDataInQueue_WhenSendCalled(self):
        # arrange
        client = Client()

        # act
        client.send("data")

        # assert
        self.assertEqual(client._sendQueue.get(), "data")
