import Queue
import logging
import select
import socket
import threading
from time import sleep

from Network.Core.Configuration import Configuration
from Threading.UnitOfWork import UnitOfWork
from Threading.UnitOfWorkThread import UnitOfWorkThread


class Server:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration=Configuration()):
        self.logger.debug("Initializing server.")

        self._configuration = configuration
        self._timeout = self._configuration.connectTimeout

        self._connection = None
        self._clientAddress = None
        self._sendQueue = Queue.Queue()
        self._connectedEvent = threading.Event()

        self.logger.debug("Creating server units of work.")
        self._acceptThread = UnitOfWorkThread(ServerAcceptUnitOfWork(self))
        self._receiveThread = UnitOfWorkThread(ServerReceiveUnitOfWork(self))
        self._sendThread = UnitOfWorkThread(ServerSendUnitOfWork(self))

        self.logger.debug("Creating server socket.")
        self._socket = socket.socket()
        self._socket.settimeout(self._timeout)

        self.receivedHandler = None

    def start(self):
        self.logger.debug("Starting server.")

        self.logger.debug("Binding server socket.")
        self._socket.bind((self._configuration.serverAddress, self._configuration.port))
        self._socket.listen(1)

        self.logger.debug("Starting accept unit of work thread.")
        self._acceptThread.start()
        self.logger.debug("Starting receive unit of work thread.")
        self._receiveThread.start()
        self.logger.debug("Starting send unit of work thread.")
        self._sendThread.start()

    def stop(self):
        self.logger.debug("Stopping server.")

        self.logger.debug("Stopping accept unit of work thread.")
        self._acceptThread.stop()
        self._acceptThread.join(self._timeout)

        self.logger.debug("Stopping receive unit of work thread.")
        self._sendThread.stop()
        self._sendThread.join(self._timeout)

        self.logger.debug("Stopping send unit of work thread.")
        self._receiveThread.stop()
        self._receiveThread.join(self._timeout)

        if not self._connection is None:
            self._connection.shutdown(socket.SHUT_RDWR)
            sleep(1)
            self._connection.close()

        self.logger.debug("Closing server socket.")
        self._socket.close()

    def send(self, data):
        self._sendQueue.put(data)

    def _clientConnected(self, connection, clientAddress):
        self._connection = connection
        self._clientAddress = clientAddress

        self._timeout = self._configuration.timeout
        self._socket.settimeout(self._timeout)

        self._connectedEvent.set()


class ServerAcceptUnitOfWork(UnitOfWork):
    def __init__(self, server):
        self._server = server

    def execute(self):
        try:
            socket = self._server._socket
            connection, clientAddress = socket.accept()
            connection.setblocking(0)
            self._server._clientConnected(connection, clientAddress)
            self._cancellationToken.cancel()
        except:
            self._server._clientConnected(None, None)
            raise


class ServerReceiveUnitOfWork(UnitOfWork):
    def __init__(self, server):
        self._server = server

    def execute(self):
        self._server._connectedEvent.wait()
        connection = self._server._connection
        if connection is None:
            return

        try:
            ready = select.select([connection], [], [], self._server._timeout)
            if ready[0]:
                data = connection.recv(self._server._configuration.packetSize)
                if not data: return
                if self._server.receivedHandler != None:
                    self._server.receivedHandler.handle(data)
        except:
            connection.close()
            raise


class ServerSendUnitOfWork(UnitOfWork):
    def __init__(self, server):
        self._server = server

    def execute(self):
        self._server._connectedEvent.wait()
        connection = self._server._connection
        if connection is None:
            return

        try:
            if not self._server._sendQueue.empty():
                data = self._server._sendQueue.get()
                connection.sendall(data)
        except:
            connection.close()
            raise
