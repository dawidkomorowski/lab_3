from Xml.XmlSerializer import XmlValue


class Configuration:
    serverAddress = XmlValue(str)
    port = XmlValue(int)
    timeout = XmlValue(float)
    rrTimeout = XmlValue(float)
    connectTimeout = XmlValue(float)
    packetSize = XmlValue(int)

    def __init__(self):
        self.serverAddress = "localhost"
        self.port = 50000
        self.timeout = 5.0
        self.rrTimeout = 120.0
        self.connectTimeout = 5.0
        self.packetSize = 1024
