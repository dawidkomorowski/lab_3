import logging
from Queue import Queue

from Network.Core.Client import Client
from Network.Core.Configuration import Configuration
from Network.Request import Request
from Network.RequestReceivedHandler import RequestReceivedHandler
from Network.Response import Response
from Threading.CancellationToken import CancellationToken
from Xml.XmlSerializer import XmlSerializer


class RRClient:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration=Configuration(), service=None):
        self.logger.debug("Initializing request-response client.")

        self._configuration = configuration
        self._client = Client(configuration)
        self._client.receivedHandler = RequestReceivedHandler(self)
        self._service = service

        self._receivedData = Queue(1)
        self._cancellationToken = CancellationToken()

    def start(self):
        self.logger.info("Starting request-response client.")
        self._cancellationToken = CancellationToken()
        self._client.start()

    def stop(self):
        self.logger.info("Stopping request-response client.")
        self._cancellationToken.cancel()
        self._client.stop()

    def run(self):
        while not self._service is None and not self._cancellationToken.isCanceled():
            if not self._receivedData.empty():
                requestData = self._receivedData.get()
                request = XmlSerializer.deserialize(requestData, Request)

                self.logger.debug("Received request from server.")

                self.logger.debug("Running client service.")
                response = self._service.serve(request)

                self.logger.debug("Sending response to server.")
                responseData = XmlSerializer.serialize(response, Response)
                self._client.send(responseData)

            if not self._service.available():
                self._cancellationToken.cancel()
