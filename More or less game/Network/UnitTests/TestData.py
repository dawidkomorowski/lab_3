from time import sleep

from Network.RRClientService import RRClientService
from Network.Response import Response


class TestRRClientService(RRClientService):
    def __init__(self):
        self.lastReceivedRequest = None
        self.receivedRequestTuple = ()

    def serve(self, request):
        self.lastReceivedRequest = request
        self.receivedRequestTuple += (request,)
        sleep(2)
        return Response.create(request.header + "\n" + request.data)

    def available(self):
        return True
