from unittest import TestCase

from Network.Request import Request
from Xml.XmlSerializer import XmlSerializer


class TestRequest(TestCase):
    def test_ShouldCreateEmptyRequest(self):
        # arrange
        # act
        request = Request.create()

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.data, "")
        self.assertEqual(request.header, "")

    def test_ShouldCreateRequest_GivenParameters(self):
        # arrange
        data = "Test data"
        header = "Test header"

        # act
        request = Request.create(data, header)

        # assert
        self.assertIsInstance(request, Request)
        self.assertEqual(request.data, data)
        self.assertEqual(request.header, header)

    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        data = "Test data"
        header = "Test header"

        request = Request.create(data, header)

        # act
        serializedRequest = XmlSerializer.serialize(request, Request)
        deserializedReqeust = XmlSerializer.deserialize(serializedRequest, Request)

        # assert
        self.assertIsInstance(deserializedReqeust, Request)
        self.assertEqual(deserializedReqeust.data, data)
        self.assertEqual(deserializedReqeust.header, header)
