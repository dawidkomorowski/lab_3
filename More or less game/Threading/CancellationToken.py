class CancellationToken:
    def __init__(self, isCanceled=False):
        self.__isCanceled = isCanceled

    def cancel(self):
        self.__isCanceled = True

    def isCanceled(self):
        return self.__isCanceled
