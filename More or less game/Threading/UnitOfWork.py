from abc import abstractmethod


class UnitOfWork:
    def __init__(self):
        self._cancellationToken = None

    @abstractmethod
    def execute(self):
        pass
