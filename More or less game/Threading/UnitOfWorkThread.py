from Threading.CancellationToken import CancellationToken
from threading import Thread


class UnitOfWorkThread(Thread):
    def __init__(self, unitOfWork):
        super(UnitOfWorkThread, self).__init__()
        self._unitOfWork = unitOfWork
        self._cancellationToken = CancellationToken()

        self._unitOfWork._cancellationToken = self._cancellationToken

    def run(self):
        while not self._cancellationToken.isCanceled():
            self._unitOfWork.execute()

    def stop(self):
        self._cancellationToken.cancel()
