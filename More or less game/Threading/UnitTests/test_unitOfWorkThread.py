from unittest import TestCase

from Threading.Common import Common
from Threading.UnitOfWorkThread import UnitOfWorkThread
from Threading.UnitTests.TestData import TestUnitOfWork


class TestUnitOfWorkThread(TestCase):
    def test_CanBeStartedAndStopped(self):
        # arrange
        unitOfWork = TestUnitOfWork()
        unitOfWorkThread = UnitOfWorkThread(unitOfWork)

        # act
        isAliveBeforeStart = unitOfWorkThread.isAlive()

        unitOfWorkThread.start()
        isAliveAfterStart = unitOfWorkThread.isAlive()

        unitOfWorkThread.stop()
        unitOfWorkThread.join(5)
        isAliveAfterStop = unitOfWorkThread.isAlive()

        # assert
        self.assertFalse(isAliveBeforeStart)
        self.assertTrue(isAliveAfterStart)
        self.assertFalse(isAliveAfterStop)

    def test_ShouldExecuteUnitOfWork(self):
        # arrange
        unitOfWork = TestUnitOfWork()
        unitOfWorkThread = UnitOfWorkThread(unitOfWork)

        # act
        unitOfWorkThread.start()
        Common.waitUntilTrue(lambda: unitOfWork.executed, 5)
        unitOfWorkThread.stop()
        unitOfWorkThread.join(5)

        # assert
        self.assertTrue(unitOfWork.executed)

    def test_ShouldBeStoppedByUnitOfWork(self):
        # arrange
        unitOfWork = TestUnitOfWork(True)
        unitOfWorkThread = UnitOfWorkThread(unitOfWork)

        # act
        unitOfWorkThread.start()
        unitOfWorkThread.join(5)
        isAlive = unitOfWorkThread.isAlive()

        # assert
        self.assertFalse(isAlive)
