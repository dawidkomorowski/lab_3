import logging

from numpy import random

from Common.RequestHeaders import RequestHeaders
from Common.RequestHelper import RequestHelper
from Network.RRServer import RRServer, TimeoutExceededException


class MoreOrLessGameServer:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration):
        self.logger.debug("Initializing game server.")

        self._server = RRServer(configuration)

    def run(self):
        self.logger.debug("Starting game server.")

        try:
            self._server.start()

            numberToGuess = random.randint(101)

            request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, "<> More or less game <>\n\nYou must guess the number from 0 to 100.\n\n")
            self._server.sendRequest(request)

            while True:
                request = RequestHelper.createRequest(RequestHeaders.AskForGameMove)
                response = self._server.sendRequest(request)

                number = int(response.data)

                if number == numberToGuess:
                    request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, "You won! " + str(numberToGuess) + " is correct number.")
                    self._server.sendRequest(request)
                    break
                if number < numberToGuess:
                    request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, "More!" + str(number) + " is lower than number to guess.")
                    self._server.sendRequest(request)
                if number > numberToGuess:
                    request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, "Less!" + str(number) + " is bigger than number to guess.")
                    self._server.sendRequest(request)

            # inform remote about end of game session
            request = RequestHelper.createRequest(RequestHeaders.EndGameSession)
            self._server.sendRequest(request)

            self._server.stop()
        except TimeoutExceededException as ex:
            self._server.stop()
            raise ex

        self.logger.debug("Closing game server.")