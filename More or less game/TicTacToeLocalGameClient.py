import logging

from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException
from Game.TicTacToeGame import TicTacToeGame
from UserInterface.CurrentPlayerCliOutputComponent import CurrentPlayerCliOutputComponent
from UserInterface.GameMessageCliOutputComponent import GameMessageCliOutputComponent
from UserInterface.GameMessageCliOutputComponentCreator import GameMessageCliOutputComponentCreator
from UserInterface.GameMoveCliInputComponent import GameMoveCliInputComponent, IncorrectGameMoveInputException
from UserInterface.GameStateCliOutputComponent import GameStateCliOutputComponent
from UserInterface.InputOutput.Readers.ConsoleReader import ConsoleReader
from UserInterface.InputOutput.Writers.ConsoleWriter import ConsoleWriter


class TicTacToeLocalGameClient:
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.logger.debug("Initializing local game.")

        self._reader = ConsoleReader()
        self._writer = ConsoleWriter()
        self._game = TicTacToeGame()

        self._gameStateCliOutputComponent = GameStateCliOutputComponent(self._game._gameState)
        self._currentPlayerCliOutputComponent = CurrentPlayerCliOutputComponent(self._game._gameState)
        self._inputDescriptionCliOutputComponent = GameMessageCliOutputComponent(
                "\nEnter number of field you want to place o/x.")
        self._incorrectInputCliOutputComponent = GameMessageCliOutputComponent(
                "Incorrect input given. Please enter correct input.\n")
        self._gameMoveCliInputComponent = GameMoveCliInputComponent()

        self._gameMessageCliOutputComponentCreator = GameMessageCliOutputComponentCreator()

    def run(self):
        self.logger.debug("Starting local game.")

        while True:
            try:
                self._currentPlayerCliOutputComponent.write(self._writer)
                self._gameStateCliOutputComponent.write(self._writer)
                self._inputDescriptionCliOutputComponent.write(self._writer)
                self._gameMoveCliInputComponent.read(self._reader)
                gameMove = self._gameMoveCliInputComponent.lastGameMove
                gameMove.gamePlayer = self._game._gameState.currentPlayer
                gameRuleException = self._game.makeMove(gameMove)
                gameMessageCliOutputComponent = self._gameMessageCliOutputComponentCreator.createGameMessage(
                        gameRuleException)
                gameMessageCliOutputComponent.write(self._writer)
                self._writer.write("")
                if isinstance(gameRuleException, PlayerWonGameRuleException) or isinstance(gameRuleException,
                                                                                           DrawGameRuleException):
                    self._gameStateCliOutputComponent.write(self._writer)
                    break
            except IncorrectGameMoveInputException:
                self._incorrectInputCliOutputComponent.write(self._writer)

        self.logger.debug("Closing local game.")
