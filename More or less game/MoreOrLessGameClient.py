import logging

from Common.RequestHeaders import RequestHeaders
from Common.RequestHelper import RequestHelper
from Common.ResponseHelper import ResponseHelper
from Common.ResponseTypes import ResponseTypes
from Network.RRClient import RRClient
from Network.RRClientService import RRClientService
from Network.RRServer import TimeoutExceededException
from Network.Response import Response, ResponseStatus


class MoreOrLessGameClient:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration):
        self.logger.debug("Initializing game client.")

        service = MoreOrLessGameClientService()
        self._client = RRClient(configuration, service)

    def run(self):
        self.logger.debug("Starting game client.")

        try:
            self._client.start()

            self.logger.debug("Starting game client service.")

            self._client.run()
            self._client.stop()
        except TimeoutExceededException as ex:
            self._client.stop()
            raise ex

        self.logger.debug("Closing game client.")

class MoreOrLessGameClientService(RRClientService):
    def __init__(self):
        self._available = True

    def serve(self, request):
        if request.header == RequestHeaders.DisplayPlainText:
            text = RequestHelper.extractData(request)
            print(text)
            return ResponseHelper.createResponse(ResponseTypes.PlainOkResponse)

        if request.header == RequestHeaders.AskForGameMove:
            while True:
                try:
                    print("Enter the number:")
                    number = int(raw_input())
                    return Response.create(str(number))
                except Exception:
                    print("It is not a number.")

        if request.header == RequestHeaders.EndGameSession:
            self._available = False
            return ResponseHelper.createResponse(ResponseTypes.PlainOkResponse)

    def available(self):
        return self._available