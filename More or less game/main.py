import logging
import sys

from Common.ConfigurationHelper import ConfigurationHelper
from MoreOrLessGameClient import MoreOrLessGameClient
from MoreOrLessGameServer import MoreOrLessGameServer

logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s: %(message)s', level=logging.DEBUG,
                    filename=__name__ + ".log")
logger = logging.getLogger(__name__)
logger.info("Logging session has started.")

if __name__ == "__main__":
    try:
        if len(sys.argv) == 1:
            print("Run parameter required:")
            print("use -s to start as game server")
            print("use -c to start as game client")
        else:
            logger.debug("Reading configuration file 'app.config'.")
            configuration = ConfigurationHelper.loadConfigurationFromFile("app.config")

            if sys.argv[1] == "-c":
                gameClient = MoreOrLessGameClient(configuration)
                gameClient.run()

            if sys.argv[1] == "-s":
                gameServer = MoreOrLessGameServer(configuration)
                gameServer.run()

        logger.info("Logging session has ended.")
    except Exception as ex:
        logger.exception(ex)
        raise ex
