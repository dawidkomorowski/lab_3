from UserInterface.CliOutputComponent import CliOutputComponent


class GameMessageCliOutputComponent(CliOutputComponent):
    def __init__(self, message):
        self._message = message

    def write(self, writer):
        writer.write(self._message)
