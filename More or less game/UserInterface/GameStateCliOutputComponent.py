from Game.GameField import GameXField, GameOField
from UserInterface.CliOutputComponent import CliOutputComponent


class GameStateCliOutputComponent(CliOutputComponent):
    def __init__(self, gameState):
        self._gameState = gameState

    def write(self, writer):
        for y in range(0, 3):
            writer.write("+---+---+---+")
            line = "|"
            for x in range(0, 3):
                chr = (x + 1) + 3 * y
                if self._gameState.getFieldAt(x, y) is GameXField:
                    chr = "x"
                if self._gameState.getFieldAt(x, y) is GameOField:
                    chr = "o"
                line += " " + str(chr) + " |"
            writer.write(line)
        writer.write("+---+---+---+")
