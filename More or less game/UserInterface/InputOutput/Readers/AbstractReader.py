from abc import abstractmethod


class AbstractReader:
    @abstractmethod
    def read(self):
        pass
