from UserInterface.InputOutput.Writers.AbstractWriter import AbstractWriter


class ConsoleWriter(AbstractWriter):
    def write(self, message):
        print(message)
