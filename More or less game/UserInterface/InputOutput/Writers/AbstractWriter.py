from abc import abstractmethod


class AbstractWriter:
    @abstractmethod
    def write(self, message):
        pass
