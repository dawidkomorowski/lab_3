from Game.GameMove import GameMove
from UserInterface.CliInputComponent import CliInputComponent


class GameMoveCliInputComponent(CliInputComponent):
    def __init__(self):
        self.lastGameMove = None

    def read(self, reader):
        input = reader.read()

        try:
            parsedInput = int(input)
        except Exception:
            raise IncorrectGameMoveInputException()

        if parsedInput < 1 or parsedInput > 9:
            raise IncorrectGameMoveInputException()

        x = (parsedInput - 1) % 3
        y = (parsedInput - 1) / 3
        position = (x, y)
        self.lastGameMove = GameMove(targetXYPosition=position)


class IncorrectGameMoveInputException(Exception):
    pass
