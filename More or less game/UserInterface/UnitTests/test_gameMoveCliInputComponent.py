from unittest import TestCase

from UserInterface.GameMoveCliInputComponent import GameMoveCliInputComponent, IncorrectGameMoveInputException
from UserInterface.InputOutput.Readers.StringReader import StringReader


class TestGameMoveCliInputComponent(TestCase):
    def test_LastGameMoveShouldBeNone_WhenNoReadExecuted(self):
        # arrange
        gameMoveCliInputComponent = GameMoveCliInputComponent()

        # act
        lastGameMove = gameMoveCliInputComponent.lastGameMove

        # assert
        self.assertIsNone(lastGameMove)

    def test_LastGameMoveShouldBeCorrect_GivenProperInput(self):
        # arrange
        gameMoveCliInputComponent = GameMoveCliInputComponent()
        stringReader = StringReader("1\n3\n5\n9")

        # act
        gameMoveCliInputComponent.read(stringReader)
        lastGameMove1 = gameMoveCliInputComponent.lastGameMove
        gameMoveCliInputComponent.read(stringReader)
        lastGameMove2 = gameMoveCliInputComponent.lastGameMove
        gameMoveCliInputComponent.read(stringReader)
        lastGameMove3 = gameMoveCliInputComponent.lastGameMove
        gameMoveCliInputComponent.read(stringReader)
        lastGameMove4 = gameMoveCliInputComponent.lastGameMove

        # assert
        self.assertEqual(lastGameMove1.targetXYPosition, (0, 0))
        self.assertEqual(lastGameMove2.targetXYPosition, (2, 0))
        self.assertEqual(lastGameMove3.targetXYPosition, (1, 1))
        self.assertEqual(lastGameMove4.targetXYPosition, (2, 2))

    def test_ReadShouldRaiseException_GivenIncorrectInput(self):
        # arrange
        gameMoveCliInputComponent = GameMoveCliInputComponent()
        stringReader = StringReader("0\n13\nnine\n!@#\n4.7")

        # act
        # assert
        with self.assertRaises(IncorrectGameMoveInputException):
            gameMoveCliInputComponent.read(stringReader)
        with self.assertRaises(IncorrectGameMoveInputException):
            gameMoveCliInputComponent.read(stringReader)
        with self.assertRaises(IncorrectGameMoveInputException):
            gameMoveCliInputComponent.read(stringReader)
        with self.assertRaises(IncorrectGameMoveInputException):
            gameMoveCliInputComponent.read(stringReader)
        with self.assertRaises(IncorrectGameMoveInputException):
            gameMoveCliInputComponent.read(stringReader)
