from unittest import TestCase

from Game.GamePlayer import GameXPlayer
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidateGameNotFinishedGameRule import GameAlreadyFinishedGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import \
    InvalidTargetPositionGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidatePlayerGameRule import InvalidPlayerGameRuleException
from UserInterface.Common import Common
from UserInterface.GameMessageCliOutputComponentCreator import GameMessageCliOutputComponentCreator


class TestGameMessageCliOutputComponentCreator(TestCase):
    def test_createGameMessage(self):
        # arrange
        gameMessageCliOutputComponentCreator = GameMessageCliOutputComponentCreator()

        # act
        noneGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(None)
        invalidPlayerGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(
                InvalidPlayerGameRuleException())
        invalidTargetPositionGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(
                InvalidTargetPositionGameRuleException())
        gameAlreadyFinishedGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(
                GameAlreadyFinishedGameRuleException())
        playerWonGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(
                PlayerWonGameRuleException(GameXPlayer))
        drawGameMessageCliOutputComponent = gameMessageCliOutputComponentCreator.createGameMessage(
                DrawGameRuleException())

        # assert
        self.assertEqual(noneGameMessageCliOutputComponent._message, GameMessageCliOutputComponentCreator.MessageNone)
        self.assertEqual(invalidPlayerGameMessageCliOutputComponent._message,
                         GameMessageCliOutputComponentCreator.MessageInvalidPlayer)
        self.assertEqual(invalidTargetPositionGameMessageCliOutputComponent._message,
                         GameMessageCliOutputComponentCreator.MessageInvalidTargetPosition)
        self.assertEqual(gameAlreadyFinishedGameMessageCliOutputComponent._message,
                         GameMessageCliOutputComponentCreator.MessageGameAlreadyFinished)
        self.assertEqual(playerWonGameMessageCliOutputComponent._message,
                         GameMessageCliOutputComponentCreator.MessagePlayerWon + Common.playerToString(
                                 GameXPlayer) + ".")
        self.assertEqual(drawGameMessageCliOutputComponent._message, GameMessageCliOutputComponentCreator.MessageDraw)
