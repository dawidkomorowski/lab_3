from unittest import TestCase

from Game.GamePlayer import GameXPlayer, GameOPlayer
from Game.GameState import GameState
from UserInterface.CurrentPlayerCliOutputComponent import CurrentPlayerCliOutputComponent
from UserInterface.InputOutput.Writers.StringWriter import StringWriter


class TestCurrentPlayerCliOutputComponent(TestCase):
    def test_write(self):
        # arrange
        stringWriter = StringWriter()
        gameState = GameState()
        currentPlayerCliOutputComponent = CurrentPlayerCliOutputComponent(gameState)

        expectedOutput = "Turn of X-player:\nTurn of O-player:\n"

        # act
        gameState.currentPlayer = GameXPlayer
        currentPlayerCliOutputComponent.write(stringWriter)

        gameState.currentPlayer = GameOPlayer
        currentPlayerCliOutputComponent.write(stringWriter)

        # assert
        self.assertEqual(stringWriter.output, expectedOutput)
