from unittest import TestCase

from Dto.GameMoveDto import GameMoveDto
from Dto.GameMoveDtoConverter import GameMoveDtoConverter
from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Game.GameMove import GameMove
from Game.GamePlayer import GameOPlayer, GameXPlayer


class TestGameMoveDtoConverter(TestCase):
    def test_fromDto(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()
        dtoConverter = GameMoveDtoConverter(gamePlayerDtoConverter)

        dto1 = GameMoveDto()
        dto1.gamePlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        dto1.targetX = 2
        dto1.targetY = 1

        dto2 = GameMoveDto()
        dto2.gamePlayer = gamePlayerDtoConverter.toDto(GameXPlayer)
        dto2.targetX = 1
        dto2.targetY = 2

        # act
        gameMove1 = dtoConverter.fromDto(dto1)
        gameMove2 = dtoConverter.fromDto(dto2)

        # assert
        self.assertIsInstance(gameMove1, GameMove)
        self.assertIs(gameMove1.gamePlayer, GameOPlayer)
        self.assertEqual(gameMove1.targetXYPosition, (2, 1))

        self.assertIsInstance(gameMove2, GameMove)
        self.assertIs(gameMove2.gamePlayer, GameXPlayer)
        self.assertEqual(gameMove2.targetXYPosition, (1, 2))

    def test_toDto(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()
        dtoConverter = GameMoveDtoConverter(gamePlayerDtoConverter)

        gameMove1 = GameMove(GameOPlayer, (2, 1))
        gameMove2 = GameMove(GameXPlayer, (1, 2))

        expectedDto1 = GameMoveDto()
        expectedDto1.gamePlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        expectedDto1.targetX = 2
        expectedDto1.targetY = 1

        expectedDto2 = GameMoveDto()
        expectedDto2.gamePlayer = gamePlayerDtoConverter.toDto(GameXPlayer)
        expectedDto2.targetX = 1
        expectedDto2.targetY = 2

        # act
        dto1 = dtoConverter.toDto(gameMove1)
        dto2 = dtoConverter.toDto(gameMove2)

        # assert
        self.assertIsInstance(dto1, GameMoveDto)
        self.assertIsInstance(dto1.gamePlayer, GamePlayerDto)
        self.assertEqual(dto1.gamePlayer.player, GamePlayerDtoConverter.OPlayer)
        self.assertEqual(dto1.targetX, 2)
        self.assertEqual(dto1.targetY, 1)

        self.assertIsInstance(dto2, GameMoveDto)
        self.assertIsInstance(dto2.gamePlayer, GamePlayerDto)
        self.assertEqual(dto2.gamePlayer.player, GamePlayerDtoConverter.XPlayer)
        self.assertEqual(dto2.targetX, 1)
        self.assertEqual(dto2.targetY, 2)
