from unittest import TestCase

from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Xml.XmlSerializer import XmlSerializer


class TestGamePlayerDto(TestCase):
    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        dto = GamePlayerDto()
        dto.player = GamePlayerDtoConverter.OPlayer

        # act
        serializedDto = XmlSerializer.serialize(dto, GamePlayerDto)
        deserializedDto = XmlSerializer.deserialize(serializedDto, GamePlayerDto)

        # assert
        self.assertIsInstance(deserializedDto, GamePlayerDto)
        self.assertEqual(deserializedDto.player, GamePlayerDtoConverter.OPlayer)
