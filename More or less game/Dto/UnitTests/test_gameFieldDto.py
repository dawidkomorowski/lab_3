from unittest import TestCase

from Dto.GameFieldDto import GameFieldDto
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Xml.XmlSerializer import XmlSerializer


class TestGameFieldDto(TestCase):
    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        dto = GameFieldDto()
        dto.field = GameFieldDtoConverter.EmptyField

        # act
        serializedDto = XmlSerializer.serialize(dto, GameFieldDto)
        deserializedDto = XmlSerializer.deserialize(serializedDto, GameFieldDto)

        # assert
        self.assertIsInstance(deserializedDto, GameFieldDto)
        self.assertEqual(deserializedDto.field, GameFieldDtoConverter.EmptyField)
