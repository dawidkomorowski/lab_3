from unittest import TestCase

from Dto.GameFieldDto import GameFieldDto
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Dto.GameStateDto import GameStateDto
from Dto.GameStateDtoConverter import GameStateDtoConverter
from Game.GameField import GameOField, GameEmptyField, GameXField
from Game.GamePlayer import GameXPlayer, GameOPlayer
from Game.GameState import GameState


class TestGameStateDtoConverter(TestCase):
    def test_fromDto(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()
        gameFieldDtoConverter = GameFieldDtoConverter()
        dtoConverter = GameStateDtoConverter(gamePlayerDtoConverter, gameFieldDtoConverter)

        dto1 = GameStateDto()
        dto1.currentPlayer = gamePlayerDtoConverter.toDto(GameXPlayer)
        dto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto1.map += (gameFieldDtoConverter.toDto(GameOField),)

        dto2 = GameStateDto()
        dto2.currentPlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        dto2.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto2.map += (gameFieldDtoConverter.toDto(GameXField),)

        expectedGameState1 = GameState()
        expectedGameState1.currentPlayer = GameXPlayer
        expectedGameState1.map = [[GameEmptyField, GameXField, GameOField],
                                  [GameEmptyField, GameXField, GameOField],
                                  [GameEmptyField, GameXField, GameOField]]

        expectedGameState2 = GameState()
        expectedGameState2.currentPlayer = GameOPlayer
        expectedGameState2.map = [[GameXField, GameEmptyField, GameOField],
                                  [GameOField, GameXField, GameEmptyField],
                                  [GameEmptyField, GameOField, GameXField]]

        # act
        gameState1 = dtoConverter.fromDto(dto1)
        gameState2 = dtoConverter.fromDto(dto2)

        # assert
        self.assertIsInstance(gameState1, GameState)
        self.assertEqual(gameState1.currentPlayer, expectedGameState1.currentPlayer)
        for x in range(0, 3):
            for y in range(0, 3):
                self.assertIs(gameState1.getFieldAt(x, y), expectedGameState1.getFieldAt(x, y))

        self.assertIsInstance(gameState2, GameState)
        self.assertEqual(gameState2.currentPlayer, expectedGameState2.currentPlayer)
        for x in range(0, 3):
            for y in range(0, 3):
                self.assertIs(gameState2.getFieldAt(x, y), expectedGameState2.getFieldAt(x, y))

    def test_toDto(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()
        gameFieldDtoConverter = GameFieldDtoConverter()
        dtoConverter = GameStateDtoConverter(gamePlayerDtoConverter, gameFieldDtoConverter)

        gameState1 = GameState()
        gameState1.currentPlayer = GameXPlayer
        gameState1.map = [[GameEmptyField, GameXField, GameOField],
                          [GameEmptyField, GameXField, GameOField],
                          [GameEmptyField, GameXField, GameOField]]

        gameState2 = GameState()
        gameState2.currentPlayer = GameOPlayer
        gameState2.map = [[GameXField, GameEmptyField, GameOField],
                          [GameOField, GameXField, GameEmptyField],
                          [GameEmptyField, GameOField, GameXField]]

        expectedDto1 = GameStateDto()
        expectedDto1.currentPlayer = gamePlayerDtoConverter.toDto(GameXPlayer)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameOField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameOField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameXField),)
        expectedDto1.map += (gameFieldDtoConverter.toDto(GameOField),)

        expectedDto2 = GameStateDto()
        expectedDto2.currentPlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameXField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameXField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameOField),)
        expectedDto2.map += (gameFieldDtoConverter.toDto(GameXField),)

        # act
        dto1 = dtoConverter.toDto(gameState1)
        dto2 = dtoConverter.toDto(gameState2)

        # assert
        self.assertIsInstance(dto1, GameStateDto)
        self.assertIsInstance(dto1.currentPlayer, GamePlayerDto)
        self.assertEqual(dto1.currentPlayer.player, expectedDto1.currentPlayer.player)
        for i in range(0, 9):
            self.assertIsInstance(dto1.map[i], GameFieldDto)
            self.assertEqual(dto1.map[i].field, expectedDto1.map[i].field)

        self.assertIsInstance(dto2, GameStateDto)
        self.assertIsInstance(dto2.currentPlayer, GamePlayerDto)
        self.assertEqual(dto2.currentPlayer.player, expectedDto2.currentPlayer.player)
        for i in range(0, 9):
            self.assertIsInstance(dto2.map[i], GameFieldDto)
            self.assertEqual(dto2.map[i].field, expectedDto2.map[i].field)
