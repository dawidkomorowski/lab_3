from unittest import TestCase

from Dto.GameFieldDto import GameFieldDto
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Game.GameField import GameEmptyField, GameXField, GameOField


class TestGameFieldDtoConverter(TestCase):
    def test_fromDto(self):
        # arrange
        dtoConverter = GameFieldDtoConverter()

        dto1 = GameFieldDto()
        dto1.field = GameFieldDtoConverter.EmptyField

        dto2 = GameFieldDto()
        dto2.field = GameFieldDtoConverter.XField

        dto3 = GameFieldDto()
        dto3.field = GameFieldDtoConverter.OField

        # act
        gameField1 = dtoConverter.fromDto(dto1)
        gameField2 = dtoConverter.fromDto(dto2)
        gameField3 = dtoConverter.fromDto(dto3)

        # assert
        self.assertIs(gameField1, GameEmptyField)
        self.assertIs(gameField2, GameXField)
        self.assertIs(gameField3, GameOField)

    def test_toDto(self):
        # arrange
        dtoConverter = GameFieldDtoConverter()

        # act
        dto1 = dtoConverter.toDto(GameEmptyField)
        dto2 = dtoConverter.toDto(GameXField)
        dto3 = dtoConverter.toDto(GameOField)

        # assert
        self.assertIsInstance(dto1, GameFieldDto)
        self.assertEqual(dto1.field, GameFieldDtoConverter.EmptyField)
        self.assertIsInstance(dto2, GameFieldDto)
        self.assertEqual(dto2.field, GameFieldDtoConverter.XField)
        self.assertIsInstance(dto3, GameFieldDto)
        self.assertEqual(dto3.field, GameFieldDtoConverter.OField)
