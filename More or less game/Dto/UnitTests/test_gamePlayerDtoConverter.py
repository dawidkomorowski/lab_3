from unittest import TestCase

from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Game.GamePlayer import GameXPlayer, GameOPlayer


class TestGamePlayerDtoConverter(TestCase):
    def test_fromDto(self):
        # arrange
        dtoConverter = GamePlayerDtoConverter()

        dto1 = GamePlayerDto()
        dto1.player = GamePlayerDtoConverter.XPlayer

        dto2 = GamePlayerDto()
        dto2.player = GamePlayerDtoConverter.OPlayer

        # act
        gamePlayer1 = dtoConverter.fromDto(dto1)
        gamePlayer2 = dtoConverter.fromDto(dto2)

        # assert
        self.assertIs(gamePlayer1, GameXPlayer)
        self.assertIs(gamePlayer2, GameOPlayer)

    def test_toDto(self):
        # arrange
        dtoConverter = GamePlayerDtoConverter()

        # act
        dto1 = dtoConverter.toDto(GameXPlayer)
        dto2 = dtoConverter.toDto(GameOPlayer)

        # assert
        self.assertIsInstance(dto1, GamePlayerDto)
        self.assertEqual(dto1.player, GamePlayerDtoConverter.XPlayer)
        self.assertIsInstance(dto2, GamePlayerDto)
        self.assertEqual(dto2.player, GamePlayerDtoConverter.OPlayer)
