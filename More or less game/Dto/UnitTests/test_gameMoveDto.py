from unittest import TestCase

from Dto.GameMoveDto import GameMoveDto
from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Game.GamePlayer import GameOPlayer
from Xml.XmlSerializer import XmlSerializer


class TestGameMoveDto(TestCase):
    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()

        dto = GameMoveDto()
        dto.gamePlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        dto.targetX = 2
        dto.targetY = 1

        # act
        serializedDto = XmlSerializer.serialize(dto, GameMoveDto)
        deserializedDto = XmlSerializer.deserialize(serializedDto, GameMoveDto)

        # assert
        self.assertIsInstance(deserializedDto, GameMoveDto)
        self.assertIsInstance(deserializedDto.gamePlayer, GamePlayerDto)
        self.assertEqual(deserializedDto.targetX, 2)
        self.assertEqual(deserializedDto.targetY, 1)
