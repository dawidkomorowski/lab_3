from abc import abstractmethod


class AbstractDtoConverter:
    @abstractmethod
    def fromDto(self, dto):
        pass

    @abstractmethod
    def toDto(self, object):
        pass
