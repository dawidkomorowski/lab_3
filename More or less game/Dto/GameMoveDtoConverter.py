from Dto.AbstractDtoConverter import AbstractDtoConverter
from Dto.GameMoveDto import GameMoveDto
from Game.GameMove import GameMove


class GameMoveDtoConverter(AbstractDtoConverter):
    def __init__(self, gamePlayerDtoConverter):
        self._gamePlayerDtoConverter = gamePlayerDtoConverter

    def fromDto(self, dto):
        gameMove = GameMove()
        gameMove.gamePlayer = self._gamePlayerDtoConverter.fromDto(dto.gamePlayer)
        gameMove.targetXYPosition = (dto.targetX, dto.targetY)

        return gameMove

    def toDto(self, object):
        dto = GameMoveDto()
        dto.gamePlayer = self._gamePlayerDtoConverter.toDto(object.gamePlayer)
        (x, y) = object.targetXYPosition
        dto.targetX = x
        dto.targetY = y

        return dto
