from Dto.AbstractDtoConverter import AbstractDtoConverter
from Dto.GamePlayerDto import GamePlayerDto
from Game.GamePlayer import GameXPlayer, GameOPlayer


class GamePlayerDtoConverter(AbstractDtoConverter):
    XPlayer = "X_PLAYER"
    OPlayer = "O_PLAYER"

    def fromDto(self, dto):
        if dto.player == GamePlayerDtoConverter.XPlayer:
            return GameXPlayer
        if dto.player == GamePlayerDtoConverter.OPlayer:
            return GameOPlayer

    def toDto(self, object):
        dto = GamePlayerDto()

        if object is GameXPlayer:
            dto.player = GamePlayerDtoConverter.XPlayer
            return dto
        if object is GameOPlayer:
            dto.player = GamePlayerDtoConverter.OPlayer
            return dto
