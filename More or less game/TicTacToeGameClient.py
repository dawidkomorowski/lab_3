import logging

from Common.RequestHeaders import RequestHeaders
from Common.RequestHelper import RequestHelper
from Common.ResponseHelper import ResponseHelper
from Common.ResponseTypes import ResponseTypes
from Network.RRClient import RRClient
from Network.RRClientService import RRClientService
from Network.RRServer import TimeoutExceededException
from UserInterface.GameMessageCliOutputComponent import GameMessageCliOutputComponent
from UserInterface.GameMoveCliInputComponent import GameMoveCliInputComponent, IncorrectGameMoveInputException
from UserInterface.GameStateCliOutputComponent import GameStateCliOutputComponent
from UserInterface.InputOutput.Readers.ConsoleReader import ConsoleReader
from UserInterface.InputOutput.Writers.ConsoleWriter import ConsoleWriter


class TicTacToeGameClient:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration):
        self.logger.debug("Initializing game client.")

        service = TicTacToeGameClientService()
        self._client = RRClient(configuration, service)

    def run(self):
        self.logger.debug("Starting game client.")

        try:
            self._client.start()

            self.logger.debug("Starting game client service.")

            self._client.run()
            self._client.stop()
        except TimeoutExceededException as ex:
            self._client.stop()
            raise ex

        self.logger.debug("Closing game client.")


class TicTacToeGameClientService(RRClientService):
    def __init__(self):
        self._reader = ConsoleReader()
        self._writer = ConsoleWriter()
        self._available = True

        self._inputDescriptionCliOutputComponent = GameMessageCliOutputComponent(
                "\nEnter number of field you want to place o/x.")
        self._incorrectInputCliOutputComponent = GameMessageCliOutputComponent(
                "Incorrect input given. Please enter correct input.\n")
        self._gameMoveCliInputComponent = GameMoveCliInputComponent()

    def serve(self, request):
        if request.header == RequestHeaders.DisplayPlainText:
            text = RequestHelper.extractData(request)
            self._writer.write(text)
            return ResponseHelper.createResponse(ResponseTypes.PlainOkResponse)

        if request.header == RequestHeaders.DisplayGameState:
            gameState = RequestHelper.extractData(request)
            gameStateCliOutputComponent = GameStateCliOutputComponent(gameState)
            gameStateCliOutputComponent.write(self._writer)
            return ResponseHelper.createResponse(ResponseTypes.PlainOkResponse)

        if request.header == RequestHeaders.AskForGameMove:
            while True:
                try:
                    self._inputDescriptionCliOutputComponent.write(self._writer)
                    self._gameMoveCliInputComponent.read(self._reader)
                    gameMove = self._gameMoveCliInputComponent.lastGameMove
                    return ResponseHelper.createResponse(ResponseTypes.GameMoveResponse, gameMove)
                except IncorrectGameMoveInputException:
                    self._incorrectInputCliOutputComponent.write(self._writer)

        if request.header == RequestHeaders.EndGameSession:
            self._available = False
            return ResponseHelper.createResponse(ResponseTypes.PlainOkResponse)

    def available(self):
        return self._available
